
new WOW().init();

var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

var fixFontResize = function(obj){
  var item = $(obj);
      fontsize = ($('.o-container').width())/1170*1;

  item.css('font-size', '1rem');

  if(isMobile){
    windowWidth = $(window).width();
  }else {
    windowWidth = $(window).width() + 18;
  }

  if(windowWidth > 602 && windowWidth < 1170) {
    item.css('font-size', fontsize+'rem');
  }
}

$('#menumobi').mmenu();

$('.block-select select').each(function(){
  var title = $(this).attr('title');
  if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
  $(this)
      .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
      .after('<span class="select">' + title + '</span>')
      .change(function(){
          val = $('option:selected',this).text();
          $(this).next().text(val);
      })
});


// Slider

var swiper = new Swiper('.swiper-banner', {
  loop:true,
  effect: 'fade'
});


function sliderSwiper(idSlider){
  var windowWidth = $(window).width(),
      nextEl = idSlider+ ' .swiper-button-next',
      prevEl = idSlider+ ' .swiper-button-prev';

  if(windowWidth > 602) {
    var slidesPerView = 3;

  } else if (windowWidth < 601 ){
    var slidesPerView = 1;
  }
  
  var swiper = new Swiper(idSlider+ ' .swiper-style', {
    slidesPerView: slidesPerView,
    spaceBetween: 27,
    loop:true,
    navigation: {
      nextEl: nextEl,
      prevEl: prevEl,
    },
  });
}

$('[data-swiper="swiper"]').each(function() {
  var idSlider = '#'+$(this)["0"].id;
  sliderSwiper(idSlider);
});

if($('.gallery-top').length) {
  var galleryTop = new Swiper('.gallery-top', {
    spaceBetween: 10,
    loopedSlides: 4
  });
  var galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 'auto',
    touchRatio: 0.2,
    slideToClickedSlide: true,
    loopedSlides: 4,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
  galleryTop.controller.control = galleryThumbs;
  galleryThumbs.controller.control = galleryTop;
}



$(window).on('resize', function(){
  fixFontResize('html');
});

$(document).ready(function() {
  fixFontResize('html');

  $("#link-popup").fancybox({
		'titlePosition'		: 'inside',
		'transitionIn'		: 'none',
		'transitionOut'		: 'none'
	});
  $("#datepicker").datepicker();
});